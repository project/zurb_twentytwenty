General TwentyTwenty plugin documentation can be found here:

http://zurb.com/playground/twentytwenty

Drupal Instructions
===================

1. Download the Zurb TwentyTwenty plugin from
   https://github.com/zurb/twentytwenty
2. Unzip the file, name the folder 'twentytwenty' and place it
   in /libraries in the root (outside of /core).
3. Enable the module

Using
=====

1. Add an image field to an entity
2. Set the image field to accept a maximum of two uploads
3. On the Manage Display setting for this field, change the formatter
   to TwentyTwenty.
4. Create content and provide two images for the field, TwentyTwenty
   will show when rendered.
