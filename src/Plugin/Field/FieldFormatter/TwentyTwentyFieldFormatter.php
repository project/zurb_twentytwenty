<?php

/**
 * @file
 * Contains Drupal\zurb_twentytwenty\Plugin\Field\FieldFormatter\TwentyTwentyFieldFormatter.
 */

namespace Drupal\zurb_twentytwenty\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\Cache;

/**
 * Plugin implementation of the 'twentytwenty_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "twentytwenty_field_formatter",
 *   label = @Translation("TwentyTwenty"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class TwentyTwentyFieldFormatter extends ImageFormatterBase implements ContainerFactoryPluginInterface {
  /**
   * The current user.
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $imageStyleStorage;

  /**
   * Constructs an TwentyTwentyFieldFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityStorageInterface $image_style_storage
   *   The image style storage entity.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, AccountInterface $current_user, EntityStorageInterface $image_style_storage) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->currentUser = $current_user;
    $this->imageStyleStorage = $image_style_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('entity_type.manager')->getStorage('image_style')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'image_style' => '',
      'default_offset_pct' => '0.5',
      'orientation' => 'horizontal',
      'before_label' => 'Before',
      'after_label' => 'After',
      'no_overlay' => false,
      'move_slider_on_hover' => false,
      'move_with_handle_only' => true,
      'click_to_move' => false
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $image_styles = image_style_options(FALSE);

    $description_link = Link::fromTextAndUrl(
      $this->t('Configure Image Styles'),
      Url::fromRoute('entity.image_style.collection')
    );

    $element['image_style'] = [
      '#title' => $this->t('Image style'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('image_style'),
      '#empty_option' => $this->t('None (original image)'),
      '#options' => $image_styles,
      '#description' => $description_link->toRenderable() + [
        '#access' => $this->currentUser->hasPermission('administer image styles')
      ],
    ];

    $element['default_offset_pct'] = [
      '#title' => $this->t('Default Offset (Percent)'),
      '#description' => $this->t('Select an offset between 0 and 1 to set how far the slider is from the lefthand side when it loads. The default is 0.5.'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('default_offset_pct'),
    ];

    $element['orientation'] = [
      '#title' => $this->t('Orientation'),
      '#description' => $this->t('The default is orientation'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('orientation'),
    ];

    $element['before_label'] = [
      '#title' => $this->t('Before label'),
      '#description' => $this->t('The default is before'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('before_label'),
    ];

    $element['after_label'] = [
      '#title' => $this->t('After label'),
      '#description' => $this->t('The default is after'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('after_label'),
    ];

    $element['no_overlay'] = [
      '#title' => $this->t('No overlay'),
      '#description' => $this->t('The default is false'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('no_overlay'),
    ];

    $element['move_slider_on_hover'] = [
      '#title' => $this->t('Move slider on hover'),
      '#description' => $this->t('The default is false'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('move_slider_on_hover'),
    ];

    $element['move_with_handle_only'] = [
      '#title' => $this->t('Move with handle only'),
      '#description' => $this->t('The default is true'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('move_with_handle_only'),
    ];

    $element['click_to_move'] = [
      '#title' => $this->t('Click to move'),
      '#description' => $this->t('The default is false'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('click_to_move'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $image_styles = image_style_options(FALSE);
    unset($image_styles['']);
    $image_style_setting = $this->getSetting('image_style');
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()->getCardinality();

    // Styles could be lost because of enabled/disabled modules that defines
    // their styles in code.
    if (isset($image_styles[$image_style_setting])) {
      $summary[] = $this->t('Image style: @value', array('@value' => $image_styles[$image_style_setting]));
    } else {
      $summary[] = $this->t('Original image');
    }

    $booleanToString = ['false', 'true'];

    $summary[] = $this->t('Default offset: @value', array('@value' => $this->getSetting('default_offset_pct')));
    $summary[] = $this->t('Orientation: @value', array('@value' => $this->getSetting('orientation')));
    $summary[] = $this->t('Before label: @value', array('@value' => $this->getSetting('before_label')));
    $summary[] = $this->t('After label: @value', array('@value' => $this->getSetting('after_label')));
    $summary[] = $this->t('No overlay: @value', array('@value' =>  $booleanToString[$this->getSetting('no_overlay')]));
    $summary[] = $this->t('Move slider on hover: @value', array('@value' => $booleanToString[$this->getSetting('move_slider_on_hover')]));
    $summary[] = $this->t('Move with handle only: @value', array('@value' => $booleanToString[$this->getSetting('move_with_handle_only')]));
    $summary[] = $this->t('Click to move: @value', array('@value' => $booleanToString[$this->getSetting('click_to_move')]));

    if ($cardinality != 2) {
      $summary[] = $this->t('This image field needs to accept two images to be able to run TwentyTwenty properly, it is currently set to %value.', array('%value' => ($cardinality > 0) ? $cardinality : 'unlimited'));
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = array();
    $images = array();
    $files = $this->getEntitiesToView($items, $langcode);

    // Early opt-out if the field is empty.
    if (empty($files)) {
      return $elements;
    }

    $image_style_setting = $this->getSetting('image_style');

    $elements['#attached']['drupalSettings']['twentytwenty']['default_offset_pct'] = $this->getSetting('default_offset_pct');
    $elements['#attached']['drupalSettings']['twentytwenty']['orientation'] = $this->getSetting('orientation');
    $elements['#attached']['drupalSettings']['twentytwenty']['before_label'] = $this->getSetting('before_label');
    $elements['#attached']['drupalSettings']['twentytwenty']['after_label'] = $this->getSetting('after_label');
    $elements['#attached']['drupalSettings']['twentytwenty']['no_overlay'] = $this->getSetting('no_overlay');
    $elements['#attached']['drupalSettings']['twentytwenty']['move_slider_on_hover'] = $this->getSetting('move_slider_on_hover');
    $elements['#attached']['drupalSettings']['twentytwenty']['move_with_handle_only'] = $this->getSetting('move_with_handle_only');
    $elements['#attached']['drupalSettings']['twentytwenty']['click_to_move'] = $this->getSetting('click_to_move');

    // Collect cache tags to be added for each item in the field.
    $cache_tags = array();
    if (!empty($image_style_setting)) {
      $image_style = $this->imageStyleStorage->load($image_style_setting);
      $cache_tags = $image_style->getCacheTags();
    }

    foreach ($files as $delta => $file) {
      /** @var \Drupal\file\FileInterface $file */
      $image_uri = $file->getFileUri();
      $url = \Drupal::service('file_url_generator')->generate($image_uri);

      $cache_tags = Cache::mergeTags($cache_tags, $file->getCacheTags());

      // Extract field item attributes for the theme function, and unset them
      // from the $item so that the field template does not re-render them.
      $item = $file->_referringItem;
      $item_attributes = $item->_attributes;
      unset($item->_attributes);

      $images[$delta] = array(
        '#theme' => 'image_formatter',
        '#item' => $item,
        '#item_attributes' => $item_attributes,
        '#image_style' => $image_style_setting,
        '#url' => $url,
        '#cache' => array(
          'tags' => $cache_tags,
        ),
      );
    }

    return array(
      '#theme' => 'zurb_twentytwenty',
      '#images' =>  \Drupal::service('renderer')->render($images),
      '#attached' => [
        'drupalSettings' => [
          'twentytwenty' => [
            'default_offset_pct' => $this->getSetting('default_offset_pct'),
            'orientation' => $this->getSetting('orientation'),
            'before_label' => $this->getSetting('before_label'),
            'after_label' => $this->getSetting('after_label'),
            'no_overlay' => $this->getSetting('no_overlay'),
            'move_slider_on_hover' => $this->getSetting('move_slider_on_hover'),
            'move_with_handle_only' => $this->getSetting('move_with_handle_only'),
            'click_to_move' => $this->getSetting('click_to_move')
          ]
        ]
      ]
    );
  }
}
