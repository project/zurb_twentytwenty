/**
 * @file
 * Attach the settings for Zurb TwentyTwenty when the page loads.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.twentytwenty = {
    attach: function (context, settings) {
      $(once('init','.twentytwenty-container', context)).twentytwenty(
        {
          default_offset_pct: drupalSettings.twentytwenty.default_offset_pct,
          before_label: drupalSettings.twentytwenty.before_label,
          after_label: drupalSettings.twentytwenty.after_label,
          no_overlay: drupalSettings.twentytwenty.no_overlay,
          move_slider_on_hover: drupalSettings.twentytwenty.move_slider_on_hover,
          move_with_handle_only: drupalSettings.twentytwenty.move_with_handle_only,
          click_to_move: drupalSettings.twentytwenty.click_to_move
        }
      );
      $(window).on('load', function () {
        $(window).trigger("resize.twentytwenty");
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
